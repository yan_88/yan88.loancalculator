﻿using System;
using Xunit;
using Yan88.LoanCalculator.BusinessLayer.Exceptions;
using Yan88.LoanCalculator.Domain;

namespace Yan88.LoanCalculator.BusinessLayer.Tests
{
    public class MonthlyLoanPaymentCalculatorTests
    {
        private const int _decimalAssertionPrecision = 2;

        private const decimal _expectedRepayment = 454.23M;

        private const decimal _expectedTotalInterest = 83523.23M;

        private const decimal _expectedTotalPayment = 163523.23M;

        [Fact]
        public void ShouldCalculateWhenInputIsValid()
        {
            //Arrange
            var input = new LoanDetails
            {
                Amount = 100000,
                Downpayment = 20000,
                InterestRate = 5.5,
                Term = 30
            };

            //Act
            var calculator = new MonthlyLoanPaymentCalculator();

            var result = calculator.Calculate(input);

            //Assert
            Assert.NotNull(result);
            Assert.Equal(_expectedRepayment, result.Repaiment, _decimalAssertionPrecision);
            Assert.Equal(_expectedTotalInterest, result.TotalInterest, _decimalAssertionPrecision);
            Assert.Equal(_expectedTotalPayment, result.TotalPayment, _decimalAssertionPrecision);
        }

        [Theory]
        [InlineData(100000)] //Equal
        [InlineData(150000)] //Greater
        public void ShouldThrowBusinessExceptionWhenDownpaymentIsGreaterOrEqualToAmount(decimal downpayment)
        {
            //Arrange
            var input = new LoanDetails
            {
                Amount = 100000,
                Downpayment = downpayment,
                InterestRate = 5.5,
                Term = 30
            };

            //Act & Assert
            var calculator = new MonthlyLoanPaymentCalculator();

            //Assert
            var exception = Assert.Throws<BusinessErrorException>(() => calculator.Calculate(input));
            Assert.NotNull(exception);
            Assert.Equal(BusinessErrors.DownpaymentIsGreaterOrEqualToAmount, exception.Message);
        }

        [Theory]
        [InlineData(-100)] //Negative
        [InlineData(0)] //Zero
        public void ShouldThrowBusinessExceptionWhenAmountIsNegativeOrZero(decimal amount)
        {
            //Arrange
            var input = new LoanDetails
            {
                Amount = amount,
                Downpayment = 200000,
                InterestRate = 5.5,
                Term = 30
            };

            //Act & Assert
            var calculator = new MonthlyLoanPaymentCalculator();

            //Assert
            var exception = Assert.Throws<BusinessErrorException>(() => calculator.Calculate(input));
            Assert.NotNull(exception);
            Assert.Equal(BusinessErrors.AmountIsNegativeOrZero, exception.Message);
        }

        [Theory]
        [InlineData(-100)] //Negative
        [InlineData(0)] //Zero
        public void ShouldThrowBusinessExceptionWhenDownpaymentIsNegativeOrZero(decimal downpayment)
        {
            //Arrange
            var input = new LoanDetails
            {
                Amount = 100000,
                Downpayment = downpayment,
                InterestRate = 5.5,
                Term = 30
            };

            //Act & Assert
            var calculator = new MonthlyLoanPaymentCalculator();

            //Assert
            var exception = Assert.Throws<BusinessErrorException>(() => calculator.Calculate(input));
            Assert.NotNull(exception);
            Assert.Equal(BusinessErrors.DownpaymentIsNegativeOrZero, exception.Message);
        }

        [Theory]
        [InlineData(-100)] //Negative
        [InlineData(0)] //Zero
        public void ShouldThrowBusinessExceptionWhenTermIsNegativeOrZero(int term)
        {
            //Arrange
            var input = new LoanDetails
            {
                Amount = 100000,
                Downpayment = 20000,
                InterestRate = 5.5,
                Term = term
            };

            //Act & Assert
            var calculator = new MonthlyLoanPaymentCalculator();

            //Assert
            var exception = Assert.Throws<BusinessErrorException>(() => calculator.Calculate(input));
            Assert.NotNull(exception);
            Assert.Equal(BusinessErrors.TermIsNegativeOrZero, exception.Message);
        }

        [Fact]
        public void ShouldThrowExceptionWhenInputIsEmpty()
        {
            //Arrange
            LoanDetails input = null;

            //Act & Assert
            var calculator = new MonthlyLoanPaymentCalculator();

            //Assert
            var exception = Assert.Throws<ArgumentNullException>(() => calculator.Calculate(input));
            Assert.NotNull(exception);
        }
    }
}
