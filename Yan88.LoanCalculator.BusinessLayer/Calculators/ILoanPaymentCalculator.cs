﻿using Yan88.LoanCalculator.Domain;

namespace Yan88.LoanCalculator.BusinessLayer
{
    /// <summary>
    /// Provides an interface for Loan payments calculation
    /// </summary>
    public interface ILoanPaymentCalculator
    {
        /// <summary>
        /// Calculates Repayment, Total Paid and Total Interest amounts based on input parameters
        /// </summary>
        /// <param name="details">Instance of <see cref="LoanDetails"/> that contains initial data for calculation</param>
        /// <returns>Calculation results, represented by <see cref="LoanPaymentDocument"/> derived class</returns>
        LoanPaymentDocument Calculate(LoanDetails details);
    }
}
