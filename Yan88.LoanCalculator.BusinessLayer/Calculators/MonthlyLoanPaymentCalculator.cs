﻿using System;
using Yan88.LoanCalculator.BusinessLayer.Exceptions;
using Yan88.LoanCalculator.Domain;

namespace Yan88.LoanCalculator.BusinessLayer
{
    /// <summary>
    /// Implements Loan monthly payment calculator
    /// </summary>
    public class MonthlyLoanPaymentCalculator : ILoanPaymentCalculator
    {
        /// <summary>
        /// Calculates Repayment, Total Paid and Total Interest amounts based on input parameters
        /// </summary>
        /// <param name="details">Instance of <see cref="LoanDetails"/> that contains initial data for calculation</param>
        /// <returns>Calculation results, represented by <see cref="LoanPaymentDocument"/> derived class</returns>
        public LoanPaymentDocument Calculate(LoanDetails details)
        {
            if (details == null)
            {
                throw new ArgumentNullException(nameof(details));
            }

            //Zero or negative amount is not allowed
            if (!(details.Amount > 0))
            {
                throw new BusinessErrorException(BusinessErrors.AmountIsNegativeOrZero);
            }

            //Zero or negative term is not allowed
            if (!(details.Term > 0))
            {
                throw new BusinessErrorException(BusinessErrors.TermIsNegativeOrZero);
            }

            //Zero or negative downpayment is not allowed
            if (!(details.Downpayment > 0))
            {
                throw new BusinessErrorException(BusinessErrors.DownpaymentIsNegativeOrZero);
            }

            //Downpayment cannot be greater than or equal to Amount as this will produce principal less than or equal to 0
            if (details.Downpayment >= details.Amount)
            {
                throw new BusinessErrorException(BusinessErrors.DownpaymentIsGreaterOrEqualToAmount);
            }

            //Calculate Repayment amount using formula M = P * (J / (1 - (1 + J)^(-N)))
            var principal = details.Amount - details.Downpayment; //Principal, P = Amount - Downpaymwnt
            var numberOfPayments = details.Term * 12; //Number of payments, N = Term(yrs) * 12
            var effectiveInterest = details.InterestRate / (100 * 12.0); //Effective interest rate, J = Interest Rate / 12
            var repayment = principal * (decimal)(effectiveInterest / (1 -  Math.Pow(1 + effectiveInterest, -1 * numberOfPayments)));

            //Calculate Total Paid amount as a sum of all payments
            var totalPaid = numberOfPayments * repayment;

            //Calculate Total Interest amount
            var totalInterest = totalPaid - principal;

            return new LoanPaymentDocument
            {
                Repaiment = repayment,
                TotalInterest = totalInterest,
                TotalPayment = totalPaid
            };

        }
    }
}
