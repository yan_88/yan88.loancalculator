﻿using System;

namespace Yan88.LoanCalculator.BusinessLayer.Exceptions
{
    /// <summary>
    /// Represents business logic error
    /// </summary>
    public class BusinessErrorException : Exception
    {
        public BusinessErrorException(string message)
            :base (message)
        {

        }
    }
}
