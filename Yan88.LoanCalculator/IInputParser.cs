﻿using System.Collections.Generic;

namespace Yan88.LoanCalculator
{
    public interface IInputParser
    {
        decimal ParseAmount(string value, string name, List<string> errors);
        int ParseTerm(string value, string name, List<string> errors);
        double ParseRate(string value, string name, List<string> errors);
    }
}