﻿using NLog;
using System;
using Yan88.LoanCalculator.IoC;

namespace Yan88.LoanCalculator
{
    class Program
    {
        private static readonly ILogger _logger = LogManager.GetLogger(typeof(Program).FullName);

        static void Main(string[] args)
        {
            try
            {
                var app = CompositionRoot.CreateApp();

                app.Process();
            }
            catch (Exception ex)
            {
                _logger.Fatal(ex);

                throw;
            }
        }
    }
}
