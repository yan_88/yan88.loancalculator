﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Yan88.LoanCalculator.Resources;

namespace Yan88.LoanCalculator
{
    /// <summary>
    /// Performs validation and parsing of user input
    /// </summary>
    public class InputParser : IInputParser
    {
        private static readonly Regex _rateRegex = new Regex(@"^[0-9]+([.][0-9]+)?%?$", RegexOptions.Compiled);
        private static readonly Regex _amountRegex = new Regex(@"^[0-9]+([.][0-9]+)?$", RegexOptions.Compiled);
        private static readonly Regex _termRegex = new Regex(@"^[0-9]+$", RegexOptions.Compiled);

        /// <summary>
        /// Converts string representstion of a rate to a double-precision floating point value. Performs user input validation
        /// </summary>
        /// <param name="value">String value of the rate</param>
        /// <param name="name">Name of the field. Is used to generate proper validation errors</param>
        /// <param name="errors">Collects validation errors if any</param>
        /// <returns>The <see cref="System.Double"/> equivalent to the string value of the rate</returns>
        public double ParseRate(string value, string name, List<string> errors)
        {
            CheckArguments(name, errors);

            //Collect all local errors 
            var validationErrors = new List<string>();
            //Validate value is not empty or null
            ValidateNotEmpty(value, name, validationErrors);

            double rate = 0.0;
            
            //Validate rate
            if (!validationErrors.Any() && !(_rateRegex.IsMatch(value) && double.TryParse(value.Trim('%'), out rate)))
            {
                validationErrors.Add(string.Format(ErrorMessages.RateHasIvalidFormat, name));
            }

            errors.AddRange(validationErrors);

            return rate;
        }

        /// <summary>
        /// Converts string representstion of an amount to a decimal value. Performs user input validation
        /// </summary>
        /// <param name="value">String value of the amount</param>
        /// <param name="name">Name of the field. Is used to generate proper validation errors</param>
        /// <param name="errors">Collects validation errors if any</param>
        /// <returns>The <see cref="System.Decimal"/> equivalent to the string value of the amount</returns>
        public decimal ParseAmount(string value, string name, List<string> errors)
        {
            CheckArguments(name, errors);

            //Collect all local errors 
            var validationErrors = new List<string>();
            //Validate value is not empty or null
            ValidateNotEmpty(value, name, validationErrors);

            decimal amount = 0.0M;

            //Validate amount
            if (!validationErrors.Any() && !(_amountRegex.IsMatch(value) && decimal.TryParse(value, out amount)))
            {
                errors.Add(string.Format(ErrorMessages.AmountHasInvalidFormat, name));
            }

            errors.AddRange(validationErrors);

            return amount;
        }

        /// <summary>
        /// Converts string representstion of a term to an integer value. Performs user input validation
        /// </summary>
        /// <param name="value">String value of the term</param>
        /// <param name="name">Name of the field. Is used to generate proper validation errors</param>
        /// <param name="errors">Collects validation errors if any</param>
        /// <returns>The <see cref="System.Int32"/> equivalent to the string value of the term</returns>
        public int ParseTerm(string value, string name, List<string> errors)
        {
            CheckArguments(name, errors);

            //Collect all local errors 
            var validationErrors = new List<string>();
            //Validate value is not empty or null
            ValidateNotEmpty(value, name, validationErrors);

            int term = 0;

            //Validate term
            if (!validationErrors.Any() && !(_termRegex.IsMatch(value) && int.TryParse(value, out term)))
            {
                errors.Add(string.Format(ErrorMessages.TermHasInvalidFormat, name));
            }

            errors.AddRange(validationErrors);

            return term;
        }

        private void ValidateNotEmpty(string value, string name, IList<string> errors)
        {
            if (string.IsNullOrEmpty(value))
            {
                errors.Add(string.Format(ErrorMessages.ValueIsNullOrEmpty, name));
            }
        }

        private void CheckArguments(string name, IList<string> errors)
        {
            if (errors == null)
            {
                throw new ArgumentNullException(nameof(errors));
            }

            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException("Empty field name is not allowed", nameof(errors));
            }
        }
    }
}
