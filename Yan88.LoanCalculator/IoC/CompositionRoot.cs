﻿using SimpleInjector;
using Yan88.LoanCalculator.BusinessLayer;
using Yan88.LoanCalculator.Serialization;

namespace Yan88.LoanCalculator.IoC
{
    /// <summary>
    // Composition root of the application
    /// </summary>
    public class CompositionRoot
    {
        private static Container _container = new Container();

        /// <summary>
        /// Registers all components and instantiates main application class
        /// </summary>
        /// <returns>Instance of <see cref="LoanCalculatorApp"/></returns>
        public static LoanCalculatorApp CreateApp()
        {
            RegisterComponents();

            return _container.GetInstance<LoanCalculatorApp>();
        }

        private static void RegisterComponents()
        {
            _container.Register<IInputParser, InputParser>();
            _container.Register<ILoanPaymentCalculator, MonthlyLoanPaymentCalculator>();
            _container.Register<IOutputSerializer, JsonOutputSerializer>();
            _container.Register<LoanCalculatorApp>();

            _container.Verify();
        }
    }
}
