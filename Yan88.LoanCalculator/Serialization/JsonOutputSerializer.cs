﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace Yan88.LoanCalculator.Serialization
{
    /// <summary>
    /// Serializes ojects to a JSON string. Implements <see cref="IOutputSerializer"/>
    /// </summary>
    public class JsonOutputSerializer : IOutputSerializer
    {
        private readonly JsonSerializer _serializer;

        /// <summary>
        /// Constructor
        /// </summary>
        public JsonOutputSerializer()
        {
            _serializer = JsonSerializer.Create(new JsonSerializerSettings { Formatting = Formatting.Indented });
            _serializer.Converters.Add(new CustomDecimalFormatConverter(2));
        }

        /// <summary>
        /// Serializes object to a json string
        /// </summary>
        /// <param name="obj">Object to serialize</param>
        /// <returns>JSON string representation of the given object</returns>
        public string Serialize(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            //Write object to the JSON string
            using (var writer = new StringWriter())
            {
                _serializer.Serialize(writer, obj);

                return writer.ToString();
            }
        }
    }
}
