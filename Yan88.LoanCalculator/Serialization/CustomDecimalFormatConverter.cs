﻿using Newtonsoft.Json;
using System;

namespace Yan88.LoanCalculator.Serialization
{
    /// <summary>
    /// Custom decimal converter to handle specific decimal fraction lengt
    /// </summary>
    public class CustomDecimalFormatConverter : JsonConverter
    {
        private readonly int _numberOfDecimals;

        public override bool CanRead => false;

        public override bool CanConvert(Type objectType) => objectType == typeof(decimal);

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="numberOfDecimals">Length of the decimal fraction</param>
        public CustomDecimalFormatConverter(int numberOfDecimals)
        {
            _numberOfDecimals = numberOfDecimals;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var d = (decimal)value;
            var rounded = Math.Round(d, _numberOfDecimals);
            writer.WriteValue((decimal)rounded);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
