﻿namespace Yan88.LoanCalculator.Serialization
{
    /// <summary>
    /// Provides an interface for serializing objects
    /// </summary>
    public interface IOutputSerializer
    {
        /// <summary>
        /// Serializes given object to a string representation
        /// </summary>
        /// <param name="obj">Object to serialize</param>
        /// <returns>String representation of the given object</returns>
        string Serialize(object obj);
    }
}