﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using Yan88.LoanCalculator.BusinessLayer;
using Yan88.LoanCalculator.BusinessLayer.Exceptions;
using Yan88.LoanCalculator.Domain;
using Yan88.LoanCalculator.Resources;
using Yan88.LoanCalculator.Serialization;

namespace Yan88.LoanCalculator
{
    /// <summary>
    /// Provides an entry point to the Loan calculator application
    /// </summary>
    public class LoanCalculatorApp
    {
        private static readonly ILogger _logger = LogManager.GetLogger(typeof(LoanCalculatorApp).FullName);
        private readonly ILoanPaymentCalculator _calculator;
        private readonly IInputParser _parser;
        private readonly IOutputSerializer _serializer;

        public LoanCalculatorApp(ILoanPaymentCalculator calculator, IInputParser parser, IOutputSerializer serializer)
        {
            _calculator = calculator;
            _parser = parser;
            _serializer = serializer;
        }

        /// <summary>
        /// Processes user input, calculates payments and outputs results
        /// </summary>
        public void Process()
        {
            do
            {
                LoanDetails input = null;

                try
                {
                    _logger.Info("Processing new request");

                    if (ProcessInput(out input))
                    {
                        //Calculate payments
                        var results = _calculator.Calculate(input);
                        //Serialize results
                        var serializedResult = _serializer.Serialize(results);
                        //Output results
                        Console.WriteLine(serializedResult);
                    }
                }
                catch (BusinessErrorException be)
                {
                    Console.WriteLine(be.Message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ErrorMessages.GenericInputError);
                    _logger.Error(ex);
                }

                Console.WriteLine($"\n{UserInterfaceStrings.ExitOrContinue}");

            } while (Console.ReadKey().Key != ConsoleKey.Escape);
        }

        private bool ProcessInput(out LoanDetails input)
        {
            input = new LoanDetails();

            Console.WriteLine();
            Console.Write($"{UserInterfaceStrings.Amount}: ");
            string amount = Console.ReadLine();
            Console.Write($"{UserInterfaceStrings.InterestRate}: ");
            string interest = Console.ReadLine();
            Console.Write($"{UserInterfaceStrings.Downpayment}: ");
            string downpaiment = Console.ReadLine();
            Console.Write($"{UserInterfaceStrings.Term}: ");
            string term = Console.ReadLine();
            Console.WriteLine();

            _logger.Debug($"Processing input: amount={amount}; interest={interest}; downpaiment={downpaiment}; term={term}");

            var errors = new List<string>();

            input.Amount = _parser.ParseAmount(amount, UserInterfaceStrings.Amount, errors);
            input.InterestRate = _parser.ParseRate(interest, UserInterfaceStrings.InterestRate, errors);
            input.Downpayment = _parser.ParseAmount(downpaiment, UserInterfaceStrings.Downpayment, errors);
            input.Term = _parser.ParseTerm(term, UserInterfaceStrings.Term, errors);

            if (errors.Any())
            {
                foreach (var error in errors)
                {
                    Console.WriteLine(error);
                }

                return false;
            }

            return true;
        }
    }
}
