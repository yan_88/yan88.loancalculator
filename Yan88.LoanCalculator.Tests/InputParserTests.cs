﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using Yan88.LoanCalculator.Resources;

namespace Yan88.LoanCalculator.Tests
{
    public class InputParserTests
    {
        private readonly InputParser _parser;

        public InputParserTests()
        {
            _parser = new InputParser();
        }

        #region Test data sources

        public static IEnumerable<object[]> InvalidRateTestData => new object[][]
            {
                new object[] { null, ErrorMessages.ValueIsNullOrEmpty },
                new object[] { "", ErrorMessages.ValueIsNullOrEmpty },
                new object[] { " ", ErrorMessages.RateHasIvalidFormat },
                new object[] { "-3.5%", ErrorMessages.RateHasIvalidFormat },
                new object[] { "-3.5", ErrorMessages.RateHasIvalidFormat },
                new object[] { "3.5 %", ErrorMessages.RateHasIvalidFormat },
                new object[] { "@#$%^&*()+_{}[]", ErrorMessages.RateHasIvalidFormat },
                new object[] { "3.", ErrorMessages.RateHasIvalidFormat },
                new object[] { ".3", ErrorMessages.RateHasIvalidFormat },
            };

        public static IEnumerable<object[]> InvalidAmountTestData => new object[][]
            {
                new object[] {"", ErrorMessages.ValueIsNullOrEmpty },
                new object[] {null, ErrorMessages.ValueIsNullOrEmpty },
                new object[] {" ", ErrorMessages.AmountHasInvalidFormat },
                new object[] {"-1000", ErrorMessages.AmountHasInvalidFormat },
                new object[] {"abc10", ErrorMessages.AmountHasInvalidFormat },
                new object[] {"@#$%^&*()+_{}[]", ErrorMessages.AmountHasInvalidFormat },
                new object[] { "300000.", ErrorMessages.AmountHasInvalidFormat },
                new object[] { ".03", ErrorMessages.AmountHasInvalidFormat },
            };

        public static IEnumerable<object[]> InvalidTermTestData => new object[][]
            {
                new object[] {"", ErrorMessages.ValueIsNullOrEmpty },
                new object[] {null, ErrorMessages.ValueIsNullOrEmpty },
                new object[] {" ", ErrorMessages.TermHasInvalidFormat },
                new object[] {"-10", ErrorMessages.TermHasInvalidFormat },
                new object[] {"abc10", ErrorMessages.TermHasInvalidFormat },
                new object[] {"@#$%^&*()+_{}[]", ErrorMessages.TermHasInvalidFormat },
                new object[] { "10.5.", ErrorMessages.TermHasInvalidFormat },
            };

        #endregion

        [Theory]
        [InlineData("5.5")]
        [InlineData("5.5%")]
        public void ShouldParseValidRate(string rate)
        {
            //Arrange
            var expected = 5.5;
            var errors = new List<string>();

            //Act
            var result = _parser.ParseRate(rate, "rate", errors);

            //Assert
            Assert.False(errors.Any());
            Assert.Equal(expected, result);
        }

        [Theory]
        [MemberData(nameof(InvalidRateTestData))]
        public void ShouldValidateInvalidRate(string rate, string errorMessage)
        {
            //Arrange
            var field = "rate";
            var expected = 0.0;
            var errors = new List<string>();

            //Act
            var result = _parser.ParseRate(rate, field, errors);

            //Assert
            Assert.Single(errors);
            Assert.Equal(string.Format(errorMessage, field), errors[0]);
            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData("", false)]
        [InlineData("", true)]
        [InlineData(null, false)]
        [InlineData(null, true)]
        public void ParseRateShouldThrowExceptionIfParametersAreInvalide(string field, bool isErrorsEmpty)
        {
            //Arrange
            var rate = "5.5%";
            var errors = isErrorsEmpty ? null : new List<string>();

            //Act
            var exception = Assert.ThrowsAny<ArgumentException>(() => _parser.ParseRate(rate, field, errors));

            //Assert
            Assert.NotNull(exception);
        }

        [Fact]
        public void ShouldParseValidAmount()
        {
            //Arrange
            var amount = "100000";
            var expected = 100000M;
            var errors = new List<string>();

            //Act
            var result = _parser.ParseAmount(amount, "amount", errors);

            //Assert
            Assert.False(errors.Any());
            Assert.Equal(expected, result);
        }

        [Theory]
        [MemberData(nameof(InvalidAmountTestData))]
        public void ShouldValidateInvalidAmount(string amount, string errorMessage)
        {
            //Arrange
            var field = "amount";
            var expected = 0.0M;
            var errors = new List<string>();

            //Act
            var result = _parser.ParseAmount(amount, "amount", errors);

            //Assert
            Assert.Single(errors);
            Assert.Equal(string.Format(errorMessage, field), errors[0]);
            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData("", false)]
        [InlineData("", true)]
        [InlineData(null, false)]
        [InlineData(null, true)]
        public void ParseAmountShouldThrowExceptionIfParametersAreInvalide(string field, bool isErrorsEmpty)
        {
            //Arrange
            var amount = "10000";
            var errors = isErrorsEmpty ? null : new List<string>();

            //Act
            var exception = Assert.ThrowsAny<ArgumentException>(() => _parser.ParseAmount(amount, field, errors));

            //Assert
            Assert.NotNull(exception);
        }

        [Fact]
        public void ShouldParseValidTerm()
        {
            //Arrange
            var term = "30";
            var expected = 30;
            var errors = new List<string>();

            //Act
            var result = _parser.ParseTerm(term, "term", errors);

            //Assert
            Assert.False(errors.Any());
            Assert.Equal(expected, result);
        }

        [Theory]
        [MemberData(nameof(InvalidTermTestData))]
        public void ShouldValidateInvalidTerm(string term, string errorMessage)
        {
            //Arrange
            var field = "term";
            var expected = 0;
            var errors = new List<string>();

            //Act
            var result = _parser.ParseTerm(term, "term", errors);

            //Assert
            Assert.Single(errors);
            Assert.Equal(string.Format(errorMessage, field), errors[0]);
            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData("", false)]
        [InlineData("", true)]
        [InlineData(null, false)]
        [InlineData(null, true)]
        public void ParseTermShouldThrowExceptionIfParametersAreInvalide(string field, bool isErrorsEmpty)
        {
            //Arrange
            var term = "30";
            var errors = isErrorsEmpty ? null : new List<string>();

            //Act
            var exception = Assert.ThrowsAny<ArgumentException>(() => _parser.ParseTerm(term, field, errors));

            //Assert
            Assert.NotNull(exception);
        }
    }
}
