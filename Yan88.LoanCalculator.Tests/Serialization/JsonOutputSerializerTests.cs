﻿using Xunit;
using Yan88.LoanCalculator.Domain;
using Yan88.LoanCalculator.Serialization;

namespace Yan88.LoanCalculator.Tests.Serialization
{
    public class JsonOutputSerializerTests
    {
        private const string _expectedJson = "{\r\n  \"monthly payment\": 521.12,\r\n  \"total interest\": 152645.22,\r\n  \"total payment\": 354562.12\r\n}";

        [Fact]
        public void ShouldSerializeToJson()
        {
            //Arrange
            var data = new LoanPaymentDocument
            {
                Repaiment = 521.123456M,
                TotalInterest = 152645.215456M,
                TotalPayment = 354562.1234568M
            };

            //Act
            var serializer = new JsonOutputSerializer();

            var result = serializer.Serialize(data);

            //Assert
            Assert.Equal(_expectedJson, result);
        }

    }
}
