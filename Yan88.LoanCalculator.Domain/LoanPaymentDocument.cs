﻿using Newtonsoft.Json;

namespace Yan88.LoanCalculator.Domain
{
    /// <summary>
    /// Represents loan payment calculation results
    /// </summary>
    public class LoanPaymentDocument
    {
        /// <summary>
        /// Monthly repayment amount
        /// </summary>
        [JsonProperty("monthly payment")]
        public decimal Repaiment { get; set; }

        /// <summary>
        /// Total interst amount
        /// </summary>
        [JsonProperty("total interest")]
        public decimal TotalInterest { get; set; }

        /// <summary>
        /// Total paiment amount
        /// </summary>
        [JsonProperty("total payment")]
        public decimal TotalPayment { get; set; }
    }
}
