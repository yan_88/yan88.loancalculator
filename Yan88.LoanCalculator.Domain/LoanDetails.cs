﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yan88.LoanCalculator.Domain
{
    public class LoanDetails
    {
        public decimal Amount { get; set; }
        public decimal Downpayment { get; set; }
        public double InterestRate { get; set; }
        public int Term { get; set; }
    }
}
